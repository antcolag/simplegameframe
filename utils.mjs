import * as crypto from "crypto";

export class Base extends EventTarget {
    /**
     * @type {String}
     */
    type = this.type || this.constructor.name

    /**
     * @type {String}
     */
    id = this.id || uuid()

    /**
     * @type {Boolean}
     */
    validate = this.validate || true

    constructor() {
        super()
        safeAssign(this, ...arguments)
        if(this.validate && !this.isValid()) {
            throw new GameError(`invalid instance of type ${
                this.constructor.name
            }`)
        }
    }

    /**
     * Verifies if the object passed as argument has the same properties
     * @param {Object}
     * @returns 
     */
    is(obj = {}){
        return verifySame(this, obj)
    }

    isValid() {
        return this.type == this.constructor.name
    }

    toJSON() {
        const result = {}
        for (const key in this) {
            if (this[key] instanceof Base) {
                continue
            }
            result[key] = this[key]
        }
        return result
    }

    async destroy() {
        this.dispatchEvent(new GameEvent("destroy", {scope: this}))
    }

    async destroyRecursive(o = {}) {
        await Promise.all(Object.values(this).map(
            async current => current instanceof Base
                && await current.destroyRecursive(o)
                && await this.destroy(o)
        ))
    }
}

export function safeAssign(obj){
	const proto = obj.constructor.prototype
	Object.assign(...arguments)
	if(proto !== Object.getPrototypeOf(obj)) {
		throw new TypeError("override __proto__")
	}
	return obj
}

function verifySame(obj, opt){
    for (const key in opt) {
        switch(true){
            case obj[key] instanceof Base && obj[key].is(opt[key]):
            case obj[key] instanceof Object && verifySame(obj[key], opt[key]):
            case obj[key] === opt[key]:
                continue;
            default:
                return false;
        }
    }
    return true
}

async function makeCryptoUuid() {
    const crypto = await import("crypto")
    return function () {
        return crypto.randomUUID()
    }
}

async function makeUrlUuid() {
    const Blob = globalThis.Blob || await import('node:buffer').Blob
    return function () {
        return globalThis.URL.createObjectURL(new Blob([])).split('/').pop()
    }
}

export const uuid = crypto ? await makeCryptoUuid() : await makeUrlUuid()

export class Notification extends Base {
    constructor() {
        super({ time: Date.now() }, ...arguments)
    }
}

export class Factory extends Base {
    default
    types = {}

    register({type, name = type.name}){
        if(!this.default) {
            this.default = name
        }
        this.types[name] = type
    }

    async create({ type }) {
        return new this.types[type || this.default](...arguments)
    }
}

export class GameEvent extends Event {
    constructor(type, data, ...tail){
        if(!(data.scope instanceof Base)) {
            throw new GameError(`${data.scope} is not a valid event scope`)
        }
        super(type)
        safeAssign(this, data, ...tail)
    }
}

export class GameError extends Error {
    constructor(msg, ...tail){
        super(msg)
        safeAssign(this, ...tail)
    }
}

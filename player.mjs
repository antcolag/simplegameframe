import * as utils from "./utils.mjs"

export class Player extends utils.Base {
    lastNotificationTimestamp = this.lastNotificationTimestamp || 0

    constructor({
        factory = new utils.Factory({})
    }){
        super({ factory }, ...arguments)
        factory.register({ type: utils.Notification })
    }

    async do({ rule, ...tail }) {
        if(await this.game.ended()){
            throw new utils.GameError("Apply rule after end")
        }
        await this.game.start()
        const args = await rule.prepare({ ...tail, player: this, game: this.game })
        return await this.game.update({ ...args, rule })
    }

    async notifications() {
        const allNotifications = await this.allNotifications()
        const newNotifications = allNotifications.filter(x => x.time > this.lastNotificationTimestamp)
        this.lastNotificationTimestamp = newNotifications[newNotifications.length - 1].time
        return newNotifications
    }

    async allNotifications() {
        const notifs = await this.game.storage.list({
            resource: `${this.game.id}/players/${this.id}/notifications`
        })
        return await Promise.all(notifs.map(async x => {
            const stored = await this.game.storage.read({
                resource: `${this.game.id}/players/${this.id}/notifications/${x}`,
                player: this
            })
            return await this.factory.create(stored)
        }))
    }

    async notify(value) {
        await this.game.storage.write({
            resource: `${this.game.id}/players/${this.id}/notifications/${value.id}`,
            player: this,
            value
        })
        this.dispatchEvent(new utils.GameEvent("notify", { scope: this }))
    }
}

import { Player } from "./player.mjs"
import { MemoryStorage } from "./storages/memory.mjs"
import * as utils from "./utils.mjs"

export const THE_GAME = Symbol('THE_GAME')

export class Game extends utils.Base {
    [THE_GAME] = new Player({game: this, id: this.id})

    constructor({
        storage = new MemoryStorage({}),
        factory = new utils.Factory({})
    }){
        super({ storage, factory }, ...arguments)
        factory.register({ type: Player })
    }

    async join(player = {}) {
        if(!(player instanceof Player)) {
            player = await this.factory.create({
                ...player,
                game: this
            })
        }

        if(!await this.canJoin(player)) {
            throw new utils.GameError("Can not join")
        }

        Object.assign(player, await this.storage.read({
            resource: `${this.id}/players/${player.id}`,
            game: this
        }, {}))

        await this.storage.write({
            resource: `${this.id}/players/${player.id}`,
            value: player,
            game: this
        })
        return player
    }

    async update({ rule, ...tail }) {
        return await rule.apply({
            ...tail,
            game: this
        })
    }

    async status(status) {
        const current =  await this.storage.read({
            resource: `${this.id}/status`,
            game: this
        }, void 0)

        if(!arguments.length || current == status) {
            return current
        }

        await this.storage.write({
            resource: `${this.id}/status`,
            value: status,
            game: this
        })

        this.dispatchEvent(new utils.GameEvent(status, {
            scope: this
        }))

        const players = await this.players()
        
        await Promise.all(
            players.map(async x => await x.notify(await x.factory.create({
                event: status
            })))
        )

        return status
    }

    async end() {
        return await this.status("ended")
    }

    async ended() {
        return await this.status() == "ended"
    }

    async start() {
        return await this.status("started")
    }

    async started() {
        return await this.status() == "started"
    }

    async players(opt = {}) {
        const list = await this.storage.list({
            resource: `${this.id}/players`
        })
        const entries = list.map(async x => {
            const player = await this.storage.read({
                resource: `${this.id}/players/${x}`,
                game: this
            })
            return await this.factory.create({ game: this }, player, opt)
        })
        return await Promise.all(entries)
    }

    async canJoin({ id, game }) {
        const isStarted = await game.started()
        const players = await game.storage.list({
            resource: `${game.id}/players`
        })
        return this.permitJoinAfterStart || players.includes(id) || !isStarted
    }
}

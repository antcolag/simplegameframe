import { GameError } from "../utils.mjs"
import * as storage from "../storage.mjs"
import * as nodefs from 'node:fs'
import * as nodepath from 'node:path'



async function readFile() {
	return new Promise((resolve, reject) => {
		nodefs.readFile(...arguments, (err, data) => {
			if (err) reject(err);
			resolve(data);
		})
	})
}
async function writeFile() {
	return new Promise((resolve, reject) => {
		nodefs.writeFile(...arguments, (err, data) => {
			if (err) reject(err);
			resolve(data);
		})
	})
}

async function open() {
	return new Promise((resolve, reject) => {
		nodefs.open(...arguments, (err, data) => {
			if (err) reject(err);
			resolve(data);
		})
	})
}

async function close() {
	return new Promise((resolve, reject) => {
		nodefs.close(...arguments, (err, data) => {
			if (err) reject(err);
			resolve(data);
		})
	})
}

async function unlink() {
	return new Promise((resolve, reject) => {
		nodefs.unlink(...arguments, (err, data) => {
			if (err) reject(err);
			resolve(data);
		})
	})
}

export class FileStorage extends storage.Storage {
	/** @returns {Boolean} */ async open(/** @type {storage.StorageArgs} */ args) {
		const file = getPath(...arguments);
		var result = false
		await lockedHandle(file, async data => {
			if (await this.acquire({
				modifier: "throw",
				...args
			})) {
				this.lock(args)
				data.resources[args.resource] ||= null
				result = true
				await super.open(...arguments)
			}
			return data
		})
		return result
	}

	/** @returns {Boolean} */ async close(/** @type {storage.StorageArgs} */ args) {
		const file = getPath(...arguments);
		var result = false
		await lockedHandle(file, async data => {
			if (await this.acquire(args)) {
				data.locks[args.resource]?.shift()
				result = true
				await super.close(...arguments)
			}
			return data
		})
		return result
	}

	async read(/** @type {storage.StorageArgs} */ args, alt) {
		const file = getPath(...arguments);
		var result;
		await lockedHandle(file, async data => {
			if (await this.acquire(args)) {
				if (args.resource in data.resources) {
					await super.read(...arguments)
					if (typeof data.resources[args.resource] !== "string") {
						result = data.resources[args.resource]
						return data
					}
					result = JSON.parse(data.resources[args.resource])
					return data
				}
				if (arguments.length > 1) {
					result = alt
					return data
				}
				throw new GameError(`resource ${args.resource} not found`);
			}
			return data;
		})
		return result
	}

	async write(/** @type {storage.StorageArgs} */ args) {
		const file = getPath(...arguments);
		var result;
		await lockedHandle(file, async data => {
			if (await this.acquire(args)) {
				await super.write(...arguments)
				result = data.resources[args.resource] = JSON.stringify(args.value)
			}
			return data
		})
		return result
	}

	async remove(/** @type {storage.StorageArgs} */ args) {
		const file = getPath(...arguments);
		await lockedHandle(file, async data => {
			if (await this.acquire(args)) {
				if (args.resource in data.resources) {
					await super.remove(...arguments)
					delete data.resources[args.resource]
				}
			}
			return data
		})
	}

	async list(/** @type {storage.StorageArgs} */ { resource }) {
		const file = getPath(...arguments);
		let json = (await readFile(file, { flag: "a+" })).toString();
		if(!json){
			json = await lockedHandle(file)
		}
		const data = JSON.parse(json)
		const result = new Set()
		for (const key in data.resources) {
			const match = key.match(new RegExp(`^${resource}/?([^/]+).*`))
			if (match) {
				result.add(match[1])
			}
		}
		return Array.from(result)
	}

	async locks(/** @type {storage.StorageArgs} */ { resource }) {
		const file = getPath(...arguments);
		let json = (await readFile(file, { flag: "a+" })).toString();
		if(!json){
			json = await lockedHandle(file)
		}
		const data = JSON.parse(json)
		return data.locks[resource];
	}

	async lock(/** @type {storage.StorageArgs} */ arg) {
		const file = getPath(...arguments);
		await lockedHandle(file, (data) => {
			(data.locks[arg.resource] ||= []).push(arg.player)
			return data
		})
	}

	async unlock(/** @type {storage.StorageArgs} */ arg) {
		const file = getPath(...arguments);
		await lockedHandle(file, async (data) => {
			data.locks[arg.resource].shift()
			await super.unlock(...arguments)
			return data
		})
	}
}

function getPath({ player }) {
	return nodepath.resolve(`${player.game.id}.game.json`)
}

async function lockedHandle(file, handler = data => data) {
	let lockHandle;
	try {
		lockHandle = await lockOpenFile(file);
		const raw = (await readFile(file, { flag: "a+" })).toString();
		const json = JSON.parse(raw || getInitDBString())
		const data = await handler(json)
		const result = JSON.stringify(data)
		if(raw != result) {
			await writeFile(file, result);
		}
		return result;
	} finally {
		await fileLockClose(lockHandle, file);
	}
}


function getInitDBString() {
	return `{
        "resources": {},
        "locks": {}
    }`
}

/** @returns {Promise<nodefs.FileHandle>} */
async function lockOpenFile(/** @type {String} */ path) {
	return new Promise(async resolve => {
		const lockfile = path + '.lock'
		//try {
			try {
				return resolve(await open(lockfile, 'ax+'));
			} catch (e) {
				let watcher = nodefs.watch(nodepath.dirname(lockfile), (event, file) => {
					watcher.close()
					if (event == 'rename' && file == (lockfile).substring(nodepath.dirname(lockfile).length + 1)) {
						return lockOpenFile(path).then(resolve)
					}
				})
			}
		// } catch(e) {
		// 	await new Promise(setImmediate)
		// 	return resolve(lockOpenFile(path))
		// }
	})
}

async function fileLockClose(/** @type {Number} */ fd, /** @type {String} */ path) {
	await close(fd);
	await unlink(`${path}.lock`)
}

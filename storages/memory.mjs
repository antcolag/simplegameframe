
import * as storage from "../storage.mjs"
import { GameError } from "../utils.mjs"

export class MemoryStorage extends storage.Storage {
    database = this.database || {
        resources: {},
        locks: {}
    }

    /** @returns {Boolean} */ async open(/** @type {storage.StorageArgs} */ args) {
        if(await this.acquire({
            modifier: "throw",
            ...args
        })) {
            this.lock(args)
            this.database.resources[args.resource] ||= null
            await super.open(...arguments)
            return true
        }
    }

    /** @returns {Boolean} */ async close(/** @type {storage.StorageArgs} */ args) {
        if(await this.acquire(args)) {
            await super.close(args)
            await this.unlock(args)
            return true
        }
    }

    async read(/** @type {storage.StorageArgs} */ args, alt) {
        if(await this.acquire(args)) {
            if(args.resource in this.database.resources) {
                await super.read(...arguments)
                if(typeof this.database.resources[args.resource] !== "string") {
                    return this.database.resources[args.resource]
                }
                return JSON.parse(this.database.resources[args.resource])
            }
            if(arguments.length > 1) {
                return alt
            }
            throw new GameError(`resource ${args.resource} not found`);
        }
    }

    async write(/** @type {storage.StorageArgs} */ args) {
        if(await this.acquire(args)) {
            await super.write(...arguments)
            return this.database.resources[args.resource] = JSON.stringify(args.value)
        }
    }

    async remove(/** @type {storage.StorageArgs} */ args) {
        if(await this.acquire(args)) {
            if(args.resource in this.database.resources) {
                await super.remove(...arguments)
                delete this.database.resources[args.resource]
            }
        }
    }

    async list(/** @type {storage.StorageArgs} */ { resource }) {
        const result = new Set()
        for (const key in this.database.resources) {
            const match = key.match(new RegExp(`^${resource}/?([^/]+).*`))
            if(match) {
                result.add(match[1])
            }
        }
        return Array.from(result)
    }

    async lock(/** @type {storage.StorageArgs} */ { resource, player }) {
        (this.database.locks[resource] ||= []).push(player)
    }

    async unlock(/** @type {storage.StorageArgs} */ { resource }) {
        this.database.locks[resource].shift()
        super.unlock(...arguments)
    }

    async locks(/** @type {storage.StorageArgs} */ { resource }) {
        return this.database.locks[resource];
    }
}

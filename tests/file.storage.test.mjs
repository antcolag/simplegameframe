import { testDriver } from "./test-storage-driver.mjs"
import { FileStorage } from "../storages/file.mjs"
import * as fs from 'node:fs/promises'
import * as path from 'node:path'

testDriver(FileStorage.name, (args) => ({
    builder: () => new FileStorage({}),
    ...args
}), data => {
    data?.game?.id && fs.unlink(path.resolve(`${data.game.id}.game.json`))
})


import { test } from "./suite.mjs"
import { Game } from "../game.mjs"
import { Storage } from "../storage.mjs"
import { Player } from "../player.mjs"

/**
 * @typedef {{
*  builder:() => Storage,
*  storage:Storage,
*  game:Game,
*  resource:string,
*  player:Player
* }} MockArgs
*/

export async function testDriver(description, /** @type {() => MockArgs} */ before, /** @type {() => void} */ after = () => {}) {
    await test(
        `${description} open base`,
        prepareTest.bind(testOpenBase, before, after)
    )
    await test(
        `${description} read base`,
        prepareTest.bind(testReadBase, before, after)
    )
    await test(
        `${description} write base`,
        prepareTest.bind(testWriteBase, before, after)
    )
    await test(
        `${description} write base`,
        prepareTest.bind(testWriteReadBase, before, after)
    )
    await test(
        `${description} list base`,
        prepareTest.bind(testListBase, before, after)
    )
    await test(
        `${description} list muti entry`,
        prepareTest.bind(testListMultipleEntry, before, after)
    )
    await test(
        `${description} list nested muti entry`,
        prepareTest.bind(testListNestedMultipleEntry, before, after)
    )
}

async function prepareTest(before, after, suite) {
    let init = await before({ suite })
    let result
    try {
        result = await this(init)
        result?.resource && await result.storage?.close(result)
    } finally {
        await after(result)
    }
}


async function testOpenBase(/** @type {MockArgs} */ arg) {
    const mocked = await mock(arg)
    const result = await mocked.storage.open(mocked)
    arg.suite.assert(result)
    return mocked
}

async function testReadBase(/** @type {MockArgs} */ arg) {
    const mocked = await mock(arg)
    await mocked.storage.open(mocked)
    const result = await mocked.storage.read(mocked)
    arg.suite.assert(result === null)
    return mocked
}

async function testWriteBase(/** @type {MockArgs} */ arg) {
    const mocked = await mock(arg)
    await mocked.storage.open(mocked)
    const value = arg.value ?? true
    const result = await mocked.storage.write({
        value,
        ...mocked
    })
    arg.suite.assert(result === JSON.stringify(value))
    return mocked
}

async function testWriteReadBase(/** @type {MockArgs} */ arg) {
    const value = 1
    const mocked = await mock(arg)
    await mocked.storage.open(mocked)
    await mocked.storage.write({ value, ...mocked })
    const result = await mocked.storage.read(mocked)
    arg.suite.assert(result === value)
    return mocked
}

async function testListBase(/** @type {MockArgs} */ arg) {
    const mocked = await mock({...arg, resource: "/path" })
    await mocked.storage.open(mocked)
    const result = await mocked.storage.list({ ...mocked, resource: "/" })
    arg.suite.assert(result.length == 1)
    return mocked
}

async function testListMultipleEntry(/** @type {MockArgs} */ arg) {
    const mocked = await mock({...arg, game: new Game({})})
    const resources = await Promise.all([
        mock({ ...mocked, resource: "/path/one-multi" }),
        mock({ ...mocked, resource: "/path/two-multi" })
    ])
    await resources[0].storage.open(resources[0])
    await resources[1].storage.open(resources[1])

    const result = await (await mock(arg)).storage.list({ ...mocked, resource: "/path/" })
    arg.suite.assert(result.length == 2)

    await resources[0].storage.close(resources[0])
    await resources[1].storage.close(resources[1])
    delete mocked.resource
    return mocked
}

async function testListNestedMultipleEntry(/** @type {MockArgs} */ arg) {
    const mocked = await  mock({...arg, game: new Game({})})
    const resources = await Promise.all([
        mock({ ...mocked, resource: "/path/one-nest" }),
        mock({ ...mocked, resource: "/path/two-nest" })
    ])
    await resources[0].storage.open(resources[0])
    await resources[1].storage.open(resources[1])

    const result = await (await mock(arg)).storage.list({ ...mocked, resource: "/" })
    arg.suite.assert(result.length == 1)

    await resources[0].storage.close(resources[0])
    await resources[1].storage.close(resources[1])
    delete mocked.resource
    return mocked
}

async function mock(/** @type {MockArgs} */ {
    builder = () => {},
    game = new Game({}),
    resource = "/resource",
    player,
    ...tail
}) {
    return {
        storage: builder(),
        game,
        resource,
        player: player ?? await game.join(),
        ...tail
    }
}

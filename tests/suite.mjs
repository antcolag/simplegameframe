/**
 * Runs a test
 * @param {String} description 
 * @param {(suite:Suite) => void} testCase 
 * @returns {Suite}
 */
export async function test(description, testCase) {
    const suite = new Suite()
    console.log(`(TEST ${suite.id}) - ${process.argv[1]}`)
    console.group();
    testsPrint(suite, description)
    console.group();
    try {
        await testCase(suite)
        suite.status = "passed"
        return suite
    } catch(e) {
        if(e == PASSED) {
            return suite;
        }
        suite.status = "failed"
        throw e
    } finally {
        console.groupEnd();
        testsPrint(suite, description)
        console.groupEnd();
    }
}

function testsPrint(suite, description) {
    const message = `[${suite.status.toUpperCase()} TEST] ${description}`
    console[suite.status === "failed" ? "error" : "log"](message)
}

export class Suite {
    /**
     * @type {Number}
     */
    static tests = 0;

    /**
     * @type {Number}
     */
    id = Suite.tests++

    /**
     * @type {String}
     */
    status = 'starting'

    fail(e) {
        console.error("test failed", e)
        this.status = "passed"
        throw e
    }

    pass() {
        suite.status = "passed"
        throw passed
    }

    /**
     * @param {Boolean} value
     * @param {String} description
     */
    assert(value, description) {
        (value === true) || this.fail(new Error(
            `assertion fail${description ? `: ${description}` : ''}`
        ))
    }
}

const PASSED = Symbol()
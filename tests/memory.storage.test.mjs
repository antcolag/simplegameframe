import { testDriver } from "./test-storage-driver.mjs"
import * as e from "../storages/memory.mjs"

testDriver(e.MemoryStorage.name, (args) => {
    const storage = new e.MemoryStorage({})
    return {
        builder: () => storage,
        ...args
    }
})


import { test } from "./suite.mjs"
import * as e from "../entities.mjs"


await test("RandomDevice", (suite) => {
    const min = 2, max = 3, next = new e.RandomDevice({ min, max }).next()
    suite.assert(next > min && next < max);
})

await test("Dice", (suite) => {
    const values = ["a", "b", "c", "d", "e", "f"], dice = new e.Dice({
        values
    }), next = dice.roll()
    suite.assert(values.some(curr => curr === next));
})
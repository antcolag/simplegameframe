import { Rule, GameError, Notification } from "../../../index.mjs"

export class RollDice extends Rule {
    async prepare({ player, game, dice }) {
        await this.begin({ player })

        try {
            const currentPlayer = await game.storage.read({
                resource: `${game.id}/currentPlayer`,
                game
            }, void 0)
    
            if(currentPlayer && !player.is(currentPlayer)) {
                throw new GameError(`Not your turn`)
            }

            return {
                value: (await game.storage.read({
                    resource: `${game.id}/players/${player.id}/score`,
                    player
                }, 0)) + dice.roll(),
                player
            }

        } catch(e) {
            await this.end({ player })
            throw e
        }
    }

    async apply({ game, player, value }) {
        try {
            const score = await game.storage.write({
                resource: `${game.id}/players/${player.id}/score`,
                value: value,
                player
            })

            await super.apply({ game, ...score })

            const rolls = await game.storage.read({
                resource: `${game.id}/rolls`,
                player
            }, 0) + 1

            await game.storage.write({
                resource: `${game.id}/rolls`,
                value: rolls,
                player
            });

            await Promise.all((await game.players()).map(
                async x => x.notify(new Notification({
                    event: "ruleplayed",
                    payload: this,
                    rolls
                })))
            )

            if(value > (game.maxScore)) {
                await game.end()
            }

            await nextPlayer({ player, game })

        } finally {
            await this.end({ player })
        }
    }
}

async function nextPlayer({ player, game }) {
    const players = await game.players()
    const currentPlayer = players.findIndex(x => x.id == player.id)
    await game.storage.write({
        resource: `${game.id}/currentPlayer`,
        value: players[(currentPlayer + 1) % players.length],
        game
    })
}

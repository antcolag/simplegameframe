import { GameError } from "../../../utils.mjs"
import { Dice } from "../../../entities.mjs"
import { Game } from "../../../game.mjs"
import { RollDice } from "./rules.mjs"
import { test } from "../../suite.mjs"

const dice = new Dice()

class DiceGame extends Game {
    constructor({ maxScore = 100, ...tail }) {
        super({ maxScore, ...tail })
    }
}

async function play(player) {
    await player.do({ rule: new RollDice(), dice })
}

await test("Example of a Dice Match with some tests", async function match({maxScore = 100}) {
    var game = new DiceGame({
        maxScore
    })

    var player1 = await game.join()
    player1.addEventListener("notify", async e => {
        // console.log("notify player1", (await e.scope.notifications()).map(x => ({...x})))
    })

    var player2 = await game.join()
    
    var player3 = await game.join()
    
    await play(player1)

    var x, e
    try {
        x = await game.join()
    } catch (err) {
        e = err
        if(!(e instanceof GameError)){
            throw e
        }
    } finally {
        if(x) {
            throw new Error("d'oh")
        }
    }
    
    await play(player2)
    await play(player3)
    await play(player1)
    
    game = new DiceGame({
        storage: game.storage,
        id: game.id
    })
    
    player2 = await game.join({
        id: player2.id
    })
    
    await play(player2)
    
    const id = game.id
    
    game = new DiceGame(game)
    
    if(id !== game.id){
        throw new Error("not copied")
    }
    
    player2 = await game.join({
        id: player2.id
    })
    
    await play(player3)
    await play(player1)
    await play(player2)
    await play(player3)
    
    try {
        x = await play(player2)
    } catch (err) {
        e = err
        if(!(e instanceof GameError)){
            throw e
        }
    } finally {
        if(x) {
            throw new Error("d'oh")
        }
    }
    
    try {
        x = await play(player3)
    } catch (err) {
        e = err
        if(!(e instanceof GameError)){
            throw e
        }
    } finally {
        if(x) {
            throw new Error("d'oh")
        }
    }
    
    try {
        for(var i = maxScore * 10; i--;) {
            x = undefined
            await play(player1)
            x = undefined
            await play(player2)
            x = undefined
            await play(player3)
        }
    } catch (err) {
        e = err
        if(!(e instanceof GameError)){
            throw e
        }
    } finally {
        if(x) {
            throw new Error("x should be undefined")
        }
    }
    
    const scores = await Promise.all([
        getPlayerSore(player1, game),
        getPlayerSore(player2, game),
        getPlayerSore(player3, game)
    ])
    
    scores.forEach(curr => {
        if(Number.isNaN(0 + curr.score)){
            throw new "type mismatch mannaggia santa"
        }
    })

    var max = game.maxScore
    const winner = scores.filter(
        curr => (0 + curr.score) > max ? max = curr.score : max
    )[0]
    
    if(!winner) {
        throw "punti sbagliati"
    }

    // console.dir(game.storage)

    // console.log("winner", winner.player.id)
    
    // console.log(await game.players())
})

async function getPlayerSore(player, game) {
    const score = await game.storage.read({
        resource: `${game.id}/players/${player.id}/score`,
        game
    })
    return {
        score,
        player
    }
}

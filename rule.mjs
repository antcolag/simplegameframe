import * as utils from "./utils.mjs"

export class Rule extends utils.Base {
    async prepare(){
        return {}
    }

    async apply({ game, ...tail }) {
        const event = new utils.GameEvent("ruleapplied", {
            scope: this,
            ...tail
        })
        this.dispatchEvent(event)
        game.dispatchEvent(event)
        return this
    }

    async begin({ player, ...tail }) {
        await player.game.storage.open({
            resource: `${player.game.id}`,
            player,
            ...tail
        })
    }

    async end({ player, ...tail }) {
        await player.game.storage.close({
            resource: `${player.game.id}`,
            player,
            ...tail
        })
    }
}

import { THE_GAME } from "./game.mjs"
import * as utils from "./utils.mjs"
import { Player } from "./player.mjs"

/**
 * @typedef {{
 * resource:String,
 * game: utils.Game,
 * player: Player,
 * modifier: String,
 * }} StorageArgs
 */

export class Storage extends utils.Base {
    /** @returns {Boolean} */ async open(/** @type {StorageArgs} */ _) {
        await fireEvent.call(this, "open", ...arguments)
    }

    /** @returns {Boolean} */ async close(/** @type {StorageArgs} */ _) {
        await fireEvent.call(this, "close", ...arguments)
    }

    async read(/** @type {StorageArgs} */ _){
        await fireEvent.call(this, "read", ...arguments)
    }

    async write(/** @type {StorageArgs} */ _){
        await fireEvent.call(this, "write", ...arguments)
    }

    async remove(){
        await fireEvent.call(this, "remove", ...arguments)
    }

    async list(/** @type {StorageArgs} */ _){}

    async lock(/** @type {StorageArgs} */ _){
        throw new utils.GameError("not implemented")
    }

    async unlock(/** @type {StorageArgs} */ _){
        await fireEvent.call(this, "unlock", ...arguments)
    }

    async locks(/** @type {StorageArgs} */ _){
        return []
    }

    /**
     * @param {StorageArgs}
     */
    async acquire({ resource, game, player = game[THE_GAME], modifier = "user", ...tail }) {
        const locks = this.locks(...arguments)
    
        if(locks && locks[0]){
            return await Storage.#policies[modifier].call(this, {
                player,
                resource, 
                locks,
                ...tail
            })
        }
        return player
    }

    static #policies = {
        throw({ resource }) {
            throw new utils.GameError(`Resource ${resource} is locked`)
        },

        user({ resource, player, locks }) {
            if(!player.is({id: locks[0].id})) {
                throw new utils.GameError(`Resource ${resource} is locked`)
            }
            return player
        },

        return() {
            return
        },

        async wait({ player }) {
            await this.lock(...arguments)
            return new Promise((ok) => {
                const handler = (evt) => {
                    if(player.is(evt.player)) {
                        evt.stopImmediatePropagation()
                        this.removeEventListener("unlock", handler)
                        ok(player)
                    }
                }
                this.addEventListener("unlock", handler)
            })
        }
    }
}

async function fireEvent(event, ...args) {
    this.dispatchEvent(new utils.GameEvent(event, {
        ...args,
        scope: this,
        event
    }))
}

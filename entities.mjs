import * as utils from "./utils.mjs";

export class RandomDevice extends utils.Base {
    /**
     * @type {Number}
     */
    min = this.min || 0

    /**
     * @type {Number}
     */
    max = this.max || 1

    /**
     * Get a random value
     * @fires RandomDevice#nextrandom
     * @returns {Number}
     */
    next() {
        let value = (Math.random() * (this.max - this.min)) + this.min
        this.dispatchEvent(new utils.GameEvent("nextrandom", {
            scope: this,
            value
        }))
        return value
    }
}

export class Dice extends utils.Base {
    /**
     * @type {Array}
     */
    values = this.values || [1,2,3,4,5,6]
    /**
     * Get a random value from the dice
     * @fires Dice#dicerolled
     * @returns {Number}
     */
    roll() {
        let value = this.values[Math.floor(Math.random() * this.values.length)]
        this.dispatchEvent(new utils.GameEvent("dicerolled", {
            scope: this,
            value
        }))
        return value
    }
}
